[Update timestep]
0.25

[Equilibriation time]
0

[Sampling timestep]
1

[Sampling time]
720

[Population size]
65293913
[ONS pop proj]

64483774
(Taken from Landscan population file)

[Fix population size at specified value]
1

[Include administrative units within countries]
1

[Mask for level 1 administrative units]
100000

[Divisor for level 1 administrative units]
1

[Divisor for countries]
1000

[Number of level 1 administrative units to include]
183

[List of level 1 administrative units to include]
44001	44002	44003	44004	44005	44006	44007	44008	44009	44010	44011	44012	44013	44014	44015	44016	44017	44018	44019	44020	44021	44022	44023	44024	44025	44026	44027	44028	44029	44030	44031	44032	44033	44034	44035	44036	44037	44038	44039	44040	44041	44042	44043	44044	44045	44046	44047	44048	44049	44050	44051	44052	44053	44054	44055	44056	44057	44058	44059	44060	44061	44062	44063	44064	44065	44066	44067	44068	44069	44070	44071	44072	44073	44074	44075	44076	44077	44078	44079	44080	44081	44082	44083	44084	44085	44086	44087	44088	44089	44090	44091	44092	44093	44094	44095	44096	44097	44098	44099	44100	44101	44102	44103	44104	44105	44106	44107	44108	44109	44110	44111	44112	44113	44114	44115	44116	44117	44118	44119	44120	44121	44122	44123	44124	44125	44126	44127	44128	44129	44130	44131	44132	44133	44134	44135	44136	44137	44138	44139	44140	44141	44142	44143	44144	44145	44146	44147	44148	44149	44150	44151	44152	44153	44154	44155	44156	44157	44158	44159	44160	44161	44162	44163	44164	44165	44166	44167	44168	44169	44170	44171	44172	44173	44174	44175	44176	44177	44178	44179	44180	44181	44182	44183


[Output incidence by administrative unit]
1

[Correct administrative unit populations]
0

[Number of spatial cells]
-1

[Grid size]	
0.075

[Use long/lat coord system]
1

[Spatial domain for simulation]	
73	6.3
136	54

[Number of micro-cells per spatial cell width]		
9	

[Proportion of network transmission]
0

[Include age]
1	

[Age distribution of population]
0.056393006	0.061635577	0.060073027	0.054560339	0.061329898	0.066922394	0.067518576	0.06574789	0.060963049	0.064146025	0.068722991	0.067173597	0.057479141	0.050111639	0.05033679	0.035952953	0.050933109
[ONS proj for 2020]

0.062 0.056 0.058 0.063 0.068 0.068 0.065 0.066 0.073 0.073 0.065 0.057 0.060 0.048 0.039 0.032 0.047

0.060217209	0.062909252	0.057928152	0.056681257	0.064513611	0.069143606	0.067567882	0.065719808	0.061143348	0.068915034	0.070793169	0.064107711	0.054836779	0.051557335	0.047376881	0.032364885	0.023616544	0.020607538

	
0	5	10	15	20	25	30	35	40	45	50	55	60	65	70	75	80

(Data from:

[Initial immunity profile by age]	
0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0

[Initial immunity applied to all household members]
1

[Relative spatial contact rates by age]
0.6	0.7	0.75	1	1	1	1	1	1	1	1	1	1	1	1	0.75	0.5
[POLYMOD, averaging 20-70]

0.25	0.25	0.5	0.75	1	1	1	1	1	1	1	1	1	0.75	0.5	0.25	0.25


[Include households]
1		

[Household size distribution]
0.294601783	0.345349339	0.154075618	0.139483899	0.045069005	0.015590461	0.003622199	0.001441591	0.000573727	0.000228319
(ONS 2020)
	
0.302836228	0.34073158	0.155126509	0.133195683	0.048757632	0.014061717	0.003267019	0.001300234	0.000517469	0.000205931

[Household attack rate]
0.14
0.0750
0.0850
(Adjusted to be the same as Cauchemez 2004 for R0=1.3.)		


[Relative transmission rates for place types]
0.14	0.14	0.10	0.07
0.15	0.15	0.15	0.075
0.175	0.175	0.175	0.0875
0.2	0.2	0.2	0.1




School=2 x workplace. This gives Longini AJE 1988 age-specific infection attack rates for R0=1.3. 
Also comparable with 1957 pandemic attack rates from Chin.

[Household transmission denominator power]		
0.8
	
(Cauchemez 2004)	

[Include places]
1

[Place overlap matrix]
1 0 0 0	
0 1 0 0
0 0 1 0
0 0 0 1

(note this isn't used - currently assume identity matrix)

[Number of types of places]
4

[Proportion of age group 1 in place types]
1	1	0	0.21

[Minimum age for age group 1 in place types]
5	11	18	16

[Maximum age for age group 1 in place types]
11	16	 65	18

[Proportion of age group 2 in place types]
0.65	 0.79	0.43	0.57

[Minimum age for age group 2 in place types]	
3	16	18	18

[Maximum age for age group 2 in place types]	
5	18	21	21

[Proportion of age group 3 in place types]
0.0075	0.0093	0.0026	0.6406

[Minimum age for age group 3 in place types]	
21	21	21	21	

[Maximum age for age group 3 in place types]	
65	65	65	65

[Kernel shape params for place types]	
3	3	3	3


[Kernel scale params for place types]	
4000	4000	4000	4000

[Mean size of place types]	
230	1010	3300	14.28

(inc teachers)

[Number of closest places people pick from (0=all) for place types]	
3	3	6	0

[Param 1 of place group size distribution]	
25	25	100	10

[Power of place size distribution]	
0	0	0	1.34

[Offset of place size distribution]
0	0	0	5.35

[Maximum of place size distribution]
0	0	0	5927

[Proportion of between group place links]	
0.25	0.25	0.25	0.25

(25% of within-group contacts)


[Include symptoms]
1

[Delay from end of latent period to start of symptoms]
0.5

assume average time to symptom onset is a day

[Proportion symptomatic by age group]
0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66 0.66
0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5

[Symptomatic infectiousness relative to asymptomatic]
1.5
2

[Relative rate of random contacts if symptomatic]
0.5

[Relative level of place attendance if symptomatic]
0.25 	0.25	0.5 	0.5
0.0 	0.0	0.0 	0.2
0.1 	0.2	0.5 	0.5
0.0 	0.0	0.0 	0.2


[Model symptomatic withdrawal to home as true absenteeism]
1

[Maximum age of child at home for whom one adult also stays at home]
16

[Proportion of children at home for whom one adult also stays at home]
1

[Duration of place absenteeism for cases who withdraw]
7

[Proportion of cases dying]
0.01

[Kernel type]
2

[Kernel scale]
4000

[Kernel Shape]
3

[Initial number of infecteds]
0

[Location of initial infecteds]
-0.176 50.835
200000	200000

[Maximum population in microcell of initial infection]
500000000

[Randomise initial infection location]
1

[All initial infections located in same microcell]
0

[Time when infection rate changes]
30

[Initial rate of importation of infections]
1


[Changed rate of importation of infections]
1


[Length of importation time profile provided]
180

[Daily importation time profile]
0.00039637	0.000422404	0.000603232	0.000806818	0.000977838	0.001215654	0.001475435	0.001759331	0.002095855	0.002476933	0.002909161	0.003410198	0.003982451	0.004638805	0.005395127	0.00626298	0.007261369	0.008410463	0.009732184	0.011253768	0.01300553	0.015022328	0.017344927	0.020019826	0.023100555	0.026649391	0.030737285	0.035446471	0.04087166	0.04712199	0.054323081	0.062619805	0.072178958	0.083192875	0.095883242	0.110505088	0.127353055	0.146765647	0.169134051	0.194907414	0.224605405	0.258825037	0.298255169	0.343688393	0.396040694	0.456363956	0.525872339	0.605965353	0.698253328	0.804594637	0.927128729	1.068321589	1.231009953	1.418469229	1.634476217	1.883366252	2.170149943	2.500595049	2.881346836	3.320054954	3.825541988	4.407960394	5.07901906	5.852193458	6.742999237	7.769281222	8.951656384	10.31379847	11.88295306	13.690484	15.7725322	18.17054397	20.93225706	24.11247717	27.77414928	31.98955641	36.84153527	42.42484495	48.84810841	56.23529325	64.72777172	74.48650501	85.69401624	98.55689323	113.307934	130.2079818	149.5482524	171.650606	196.8670786	225.5805289	258.1996665	295.1479906	336.8602628	383.7567184	436.226154	494.5852818	559.0401758	629.6271715	706.1466306	788.0877032	874.5385994	964.1277368	1054.942274	1144.497997	1229.819016	1307.478109	1373.849859	1425.375658	1458.909049	1472.112189	1463.74025	1433.873478	1383.937547	1316.559006	1235.24025	1143.978092	1046.827852	947.5742692	849.4524822	755.0099762	666.1067926	583.9342875	509.1260534	441.8703774	382.0284161	329.2333209	282.9819783	242.6943429	207.7671547	177.6025491	151.6341438	129.3333359	110.223406	93.87499488	79.90856345	67.99027638	57.82900897	49.17215405	41.80131863	35.5286855	30.19272742	25.65482116	21.79680064	18.51741927	15.73036742	13.3620726	11.34984238	9.640307581	8.188006869	6.954337378	5.906435165	5.016354116	4.260368274	3.61826508	3.072912654	2.609742089	2.216371142	1.882286966	1.598554546	1.357591285	1.152944119	0.979149081	0.831549648	0.70620165	0.59974816	0.509342763	0.43256591	0.367362791	0.311990061	0.264964778	0.22502858	0.19111364	0.162311178	0.137852214	0.117080312	0.099440002	0.084459683	0.07173828	0.06093513	0.051761119
0


[Assume SI model]
0

[Reproduction number]
2

[Power of scaling of spatial R0 with density]
0

[Include latent period]
1

[Latent period]	
4.59

(From Marc's estimates) - minus half a day to account for infectiousness pre symptom onset

[Latent period inverse CDF]
0	0.098616903	0.171170649	0.239705594	0.307516598	0.376194441	0.446827262	0.520343677	0.597665592	0.679808341	0.767974922	0.863671993	0.968878064	1.086313899	1.219915022	1.37573215	1.563841395	1.803041398	2.135346254	2.694118208	3.964172493



(From Marc's estimates)

0.337603919	0.531028655	0.605598307	0.663734057	0.714098988	0.760017037	0.803220894	0.844790662	0.885497063	0.925959562	0.966735451	1.008383094	1.05152112	1.096901152	1.145518376	1.19880817	1.25904732	1.330312414	1.421351091	1.55855824	2.120232776


1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1
(fixed latent period)

0	0.084316056	0.179208436	0.287716206	0.414411663	0.566644354	0.757376134	0.806831536	0.859743301	0.916631096	0.978141381	1.04509234	1.118540666	1.199884333	1.291027502	1.394658862	1.514752269	1.645029341	1.802468475	2.001461849	2.272128403

(Elveback)

[Model time varying infectiousness]
1

[Infectiousness profile]
0.487464241	1	1.229764827	1.312453175	1.307955665	1.251658756	1.166040358	1.065716869	0.960199498	0.855580145	0.755628835	0.662534099	0.577412896	0.500665739	0.432225141	0.371729322	0.318643018	0.272340645	0.232162632	0.19745264	0.167581252	0.141960133	0.120049578	0.101361532	0.085459603	0.071957123	0.060514046	0.050833195	0.04265624	0.035759641	0.029950735	0.025064045	0.02095788	0.017511251	0.014621091	0.012199802	0.010173075	0.008477992	0.007061366	0.005878301	0.00489096	0.004067488	0.003381102	0.00280931	0.002333237	0.001937064	0.001607543	0.001333589	0.001105933	0.00091683	0.000759816	0.000629496	0.000521372	0.000431695	0.000357344	0.000295719	0.000244659



10.66332076	4.541186908	2.687577454	1.827631772	1.340883681	1.033008677	0.823681092	0.673874701	0.562464626	0.47708646	0.410059783	0.356384398	0.312678622	0.276582011	0.246402478	0.220899078	0.199143584	0.18042958	0.164211194	0.150060852	0.137639543	0.126675513	0.11694875	0.108279517	0.100519754	0.093546572	0.087257254	0.081565386


[Infectious period]
14

[Infectious period inverse CDF]	
0	0.171566836	0.424943468	0.464725594	0.50866631	0.55773764	0.613298069	0.67732916	0.752886568	0.843151261	0.895791527	0.955973422	1.026225109	1.110607115	1.216272375	1.336349102	1.487791911	1.701882384	1.865779085	2.126940581	2.524164972

(Cauchemez)

[k of individual variation in infectiousness]
0.25

===================================
[Include holidays]
1

[Proportion of places remaining open during holidays by place type]
0	0	1	1

[Number of holidays]
5

[Holiday start times]
23	78	127	176	234

[Holiday durations]
14	7	14	7	42

===================================


[Target country]	
44
	
[Restrict treatment to target country]
0


[Use global triggers for interventions]
1

[Use admin unit triggers for interventions]
0

[Number of sampling intervals over which cumulative incidence measured for global trigger]
7

[Use cases per thousand threshold for area controls]
0

[Divisor for per-capita global threshold (default 1000)]
100000

[Divisor for per-capita area threshold (default 1000)]
1000


[Proportion of cases detected before outbreak alert]
1

[Number of detected cases needed before outbreak alert triggered]
100

[Proportion of cases detected for treatment]
1

[Treatment trigger incidence per cell]
100

[Places close only once]
0

[Social distancing only once]
0


[Use ICU case triggers for interventions]
1

====================================

[Number of realisations]
1

[Number of non-extinct realisations]
1

[Maximum number of cases defining small outbreak]
10000

[Do one generation]
0

[Output every realisation]
1

[Output bitmap]
0

[Output infection tree]
0

[Only output non-extinct realisations]
0

[Bitmap scale]
60

[Bitmap y:x aspect scaling]
1.5

[Calculate spatial correlations]
0

[Bitmap movie frame interval]
300

[Record infection events]
0

[Record infection events per run]
0

[Max number of infection events to record]
10000000000

[Limit number of infections]
0

[Max number of infections]
10000000000


===================================																				

[Do Severity Analysis]																				
1																				
																				
[Mean_MildToRecovery]																				
10																				
																				
[Mean_ILIToRecovery]																				
14

[Mean_SARIToRecovery]																				
8																				
																				
[Mean_CriticalToCritRecov]																				
10																				
																				
[Mean_CritRecovToRecov]																				
5																				
																				
[Mean_ILIToSARI]																				
5																				
																				
[Mean_SARIToCritical]																				
6																				
																				
[Mean_CriticalToDeath]																				
10																				
																				
[Mean_SARIToDeath]																				
8																				
																				
[MildToRecovery_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1

																				
[ILIToRecovery_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																			
[SARIToRecovery_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																			
[CriticalToCritRecov_icdf]																			1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																				
[CritRecovToRecov_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																				
[ILIToSARI_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																				
[SARIToCritical_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																				
[CriticalToDeath_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																				
[SARIToDeath_icdf]																				
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1																				
[Prop_Mild_ByAge]																				
0.642962245	0.642962245	0.628705377	0.628705377	0.579207151	0.579207151	0.540447477	0.540447477	0.511295428	0.511295428	0.421230829	0.421230829	0.342452284	0.342452284	0.267218361	0.267218361	0.234989185
																				
[Prop_ILI_ByAge]																				
0.356293563	0.35640359	0.370123515	0.368900031	0.415446412	0.410502964	0.443317919	0.436203354	0.459759949	0.45009753	0.521034292	0.506347036	0.555945257	0.540567901	0.586682575	0.556146985	0.585010815
																					
[Prop_SARI_ByAge]																				
0.001070934	0.0009126	0.001685292	0.003445953	0.007693823	0.014807722	0.023362506	0.033494916	0.041235376	0.054131584	0.078413377	0.093333699	0.119494142	0.122831301	0.135965201	0.144193328	0.079242424

																			
[Prop_Critical_ByAge]																				
5.66296E-05	4.82571E-05	8.9116E-05	0.000182217	0.000406839	0.000783013	0.001235379	0.001882613	0.002620114	0.004363934	0.009063713	0.016396809	0.034448976	0.054410842	0.085397018	0.123434935	0.193484848

							
[CFR_Critical_ByAge]																				
0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5				
																				
[CFR_SARI_ByAge]																				
0.013447578	0.013447578	0.013447578	0.013447578	0.013447578	0.013447578	0.013447578	0.014293732	0.016158969	0.020501748	0.029395403	0.044677032	0.073315052	0.112652243	0.159727019	0.21769897	0.620944315

				
																				
	



