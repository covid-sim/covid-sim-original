Original report 9 code
======================

This code will not work without an input population file which is commercially licensed. We will release that underlying population file to individuals who provide written proof that they have a license for Landscan 2018 - see https://landscan.ornl.gov/landscan-data-availability.

The code will only exactly reproduce report 9 results if run on a 32-core Windows machine with the network file provided.

To do so, run batch.ps1 in either the suppress or mitigation directories on a Windows HPC2016 cluster. Paths in the script will require editing. You can use the executable provided or compile yourself (x64-release configuration on Microsoft Visual Studio 2019).

Then follow the instructions at https://github.com/mrc-ide/covid-sim/tree/master/report9







